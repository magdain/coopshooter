﻿using UnityEngine;
using System.Linq;

public class NetworkManager : MonoBehaviour
{
    public GameObject Player1Prefab;
    public GameObject Player2Prefab;
    public Unit Player1;
    public Unit Player2;

    private const string _gameName = "MMCoopShooter_x389403kp";
    private const string _roomName = "Room 1";
    private const int _port = 25444;

    public Texture2D HealthFill;
    public Texture2D HealthOverlay;

    public string _ip = "127.0.0.1";
    public string _guid = "";
    void OnGUI()
    {
        //MasterServer.RequestHostList(_gameName);
        //var hosts = MasterServer.PollHostList();

        int x = 5, y = 0;
        if (!Network.isServer && !Network.isClient)
        {
            GUI.color = Color.red;
            GUI.Label(new Rect(x, y, 200, 100), "NOT CONNECTED");

            GUI.color = Color.yellow; y += 20;
            if (GUI.Button(new Rect(x, y, 110, 20), "Host Game"))
            {
                //Debug.Log("Initializing server on: " + _port);
                var error = Network.InitializeServer(2, _port, true); Debug.Log(error);
                //MasterServer.RegisterHost(_gameName, _roomName);
                Player1 = CreatePlayer(Player1Prefab).GetComponent<Unit>();
            }

            GUI.color = Color.green; y += 40;
            _ip = GUI.TextField(new Rect(x, y, 110, 20), _ip); y += 20;
            if (GUI.Button(new Rect(x, y, 110, 20), "Join By IP"))
            {
                //Debug.Log("Attempting connection to: " + _ip);

                var error = Network.Connect(_ip, _port); Debug.Log(error);
            }

            //*
            GUI.color = Color.magenta; y += 40;
            _guid = GUI.TextField(new Rect(x, y, 110, 20), _guid); y += 20;
            if (GUI.Button(new Rect(x, y, 110, 20), "Join By GUID"))
            {
                var error = Network.Connect(_guid); y += 20; Debug.Log(error);
            }
            //*/
        }
        else
        {
            GUI.color = Color.green;
            GUI.Label(new Rect(x, y, 200, 100), Network.isServer ? "HOST" : "CLIENT"); y += 20;
            GUI.Label(new Rect(x, y, 500, 100), "External IP -- " + Network.player.externalIP); y += 20;
            GUI.Label(new Rect(x, y, 500, 100), "IP -- " + Network.player.ipAddress); y += 20;
            GUI.Label(new Rect(x, y, 500, 100), "Port -- " + Network.player.externalPort); y += 20;
            GUI.Label(new Rect(x, y, 500, 100), "GUID -- ");
            GUI.TextField(new Rect(55, y, 200, 20), Network.player.guid);
        }
    }

    void OnPlayerConnected(NetworkPlayer player)
    {
        GetComponent<NetworkView>().RPC("SpawnPlayer", player);
    }
    void OnFailedToConnect(NetworkConnectionError error)
    {
        Debug.Log(string.Format("Couldn't connect to server @ {0}: {1}", _ip, error));
    }

    [RPC]
    void SpawnPlayer()
    {
        Player2 = CreatePlayer(Player2Prefab).GetComponent<Unit>();
    }
    private GameObject CreatePlayer(GameObject prefab)
    {
        var unit = GameObject.Find("Unit");
        var player = Network.Instantiate(prefab, Vector3.up, Quaternion.identity, 0) as GameObject;
        player.transform.parent = unit.transform;
        return player;
    }
}
