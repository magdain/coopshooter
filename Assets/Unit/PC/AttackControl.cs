﻿using UnityEngine;

public class AttackControl : MonoBehaviour
{
    public GameObject ProjectilePrefab;
    void Start()
    {
        enabled = GetComponent<NetworkView>().isMine;
    }

    public float ProjectilesPerSecond = 4f;
    private float _projectileTick = 0f;
    void Update()
    {
        _projectileTick += Time.deltaTime;
        if (Input.GetMouseButton(0))
        {
            if (_projectileTick >= 1f / ProjectilesPerSecond)
            {
                var plane = new Plane(Vector3.up, transform.position);
                var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                float distance;

                if (plane.Raycast(ray, out distance))
                {
                    var point = ray.GetPoint(distance);
                    var go = Instantiate(ProjectilePrefab);
                    go.transform.position = transform.position;

                    var proj = go.GetComponent<Projectile>();
                    proj.Direction = point - transform.position;
                }

                _projectileTick = 0f;
            }
        }
    }
}
