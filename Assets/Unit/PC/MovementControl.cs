﻿using UnityEngine;

[RequireComponent(typeof(CharacterController))]
public class MovementControl : MonoBehaviour
{
    public float Speed;

    protected CharacterController _controller;

    void Start()
    {
        enabled = GetComponent<NetworkView>().isMine;
        _controller = _controller ?? GetComponent<CharacterController>();
    }

    void Update()
    {
        var delta = Vector3.zero;

        if (Input.GetKey(KeyCode.W))
            delta += Vector3.forward;
        if (Input.GetKey(KeyCode.S))
            delta += Vector3.forward * -1;

        if (Input.GetKey(KeyCode.D))
            delta += Vector3.right;
        if (Input.GetKey(KeyCode.A))
            delta += Vector3.right * -1;


        _controller.Move(delta.normalized * Speed * Time.deltaTime);
    }
}
