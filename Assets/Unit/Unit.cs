﻿using UnityEngine;

public class Unit : MonoBehaviour 
{
    public int Health;
    public int MaxHealth;

    public void Damage(int amount)
    {
        var newHealth = Health - amount;
        if (newHealth <= 0)
        {
            OnDeath();
            return;
        }

        Health = newHealth;
    }
    protected void OnDeath()
    {
        gameObject.layer = 11; // It's just a number. Guess what it does.
                                    // (Other than make me hate Unity)
                                    //
                                    // (For realsies though, it's InactiveEnemy)
    }
}
