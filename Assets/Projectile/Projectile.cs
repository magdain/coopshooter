﻿using UnityEngine;

public class Projectile : MonoBehaviour
{
    public int Damage;

    public float Speed;
    public Vector3 Direction;
    void Update()
    {
        transform.position += Direction.normalized * Speed * Time.deltaTime;
    }

    void OnTriggerEnter(Collider c)
    {
        var unit = c.GetComponent<Unit>();
        if (unit != null)
        {
            unit.Damage(Damage);
            Destroy(gameObject);
        }
    }
}
